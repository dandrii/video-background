// Video Background.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <filesystem>
#include <format>
#include <windows.h>
#include <wchar.h>

BOOL CALLBACK findWorker(HWND wnd, LPARAM lp)
{
	HWND* worker = (HWND*)lp;

	if (!FindWindowExA(wnd, 0, "SHELLDLL_DefView", 0))
	{
		return TRUE;
	}

	*worker = FindWindowExA(0, wnd, "WorkerW", 0);
	if (*worker)
	{
		return FALSE;
	}

	return TRUE;
}

std::string GetLastErrorAsString()
{
	DWORD errorMessageID = ::GetLastError();
	if (errorMessageID == 0) {
		return nullptr;
	}

	LPSTR messageBuffer = nullptr;

	size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);

	std::string message(messageBuffer, size);

	LocalFree(messageBuffer);

	return message;
}

std::wstring s2ws(const std::string& s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}

void lerr(const char* msg)
{
	char title[50];
	sprintf_s(title, "%s [GLE=%#010x]", msg, GetLastError());
	std::string content = GetLastErrorAsString();
	MessageBoxA(NULL, content.c_str(), title, MB_OK | MB_ICONERROR);
}

HWND createWorker()
{
	// getting handle for background
	HWND progman = FindWindowA("Progman", 0);
	if (!progman)
	{
		lerr("SetWindowPos failed");
		std::cerr << "No Programs Manager GLE=0x" << std::hex << GetLastError() << std::endl;
		return nullptr;
	}

	// undocumented hack
	SendMessageA(progman, 0x052C, 0xD, 0);
	SendMessageA(progman, 0x052C, 0xD, 1);

	HWND worker = nullptr;
	EnumWindows(findWorker, (LPARAM)&worker);

	if (!worker) {
		SendMessageA(progman, 0x052C, 0, 0);
		EnumWindows(findWorker, (LPARAM)&worker);
	}

	if (!worker)
	{
		std::cerr << "Could not spawn worker" << std::endl;
		worker = progman;
	}

	return worker;
}

int main()
{
	HWND worker = createWorker();

	// kill existing window if available
	HWND wnd = FindWindowExA(worker, NULL, "MediaPlayerClassicW", 0);
	if (wnd)
	{
		SendMessageA(wnd, WM_CLOSE, 0, 0);
		return 0;
	}

	TCHAR szFileName[MAX_PATH];

	GetModuleFileName(NULL, szFileName, MAX_PATH);
	std::filesystem::path exe(szFileName);
	std::filesystem::path dir = exe.remove_filename();
	std::filesystem::path mpcPath = dir / "mpc-hc64.exe";
	std::filesystem::path mpvPath = dir / "mpv.exe";

	std::filesystem::path video = dir / "video.mp4";

	std::string tmp;
	LPCSTR cmd;
	LPCSTR args;
	LPCSTR cls;

	if (std::filesystem::exists(mpcPath))
	{
		tmp = mpcPath.string();
		cmd = tmp.c_str();
		tmp = std::format("/nofocus /nocrashreporter /fullscreen /play \"{}\"", video.string());
		args = tmp.c_str();
		cls = "MediaPlayerClassicW";
	}
	else if (std::filesystem::exists(mpvPath))
	{
		tmp = mpvPath.string();
		cmd = tmp.c_str();
		tmp = std::format("--player-operation-mode=pseudo-gui --force-window=yes --terminal=no --loop=inf --no-audio \"{}\"", video.string());
		args = tmp.c_str();
		cls = "mpv";
	}
	else
	{
		lerr("No player found");
		return 1;
	}

	// opening player with a video
	if (!ShellExecuteA(0, "open", cmd, args, 0, 0))
	{
		lerr("ShellExecuteA failed");
		return 1;
	}

	Sleep(1000);


	// getting handle for the opened player window
	wnd = FindWindowA(cls, 0);

	// setting size fot the player window
	RECT rect;
	GetWindowRect(wnd, &rect);

	long dx = 1920 - rect.left;
	rect.left += dx;
	rect.right += dx;

	if (!SetWindowPos(wnd, 0, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, 0))
	{
		lerr("SetWindowPos failed");
		return 1;
	}

	// moving spawned npv to the background
	char wndclass[512];
	*wndclass = 0;
	GetClassNameA(wnd, wndclass, sizeof(wndclass) - 1);

	long ws_and = ~(
		WS_CAPTION |
		WS_THICKFRAME |
		WS_SYSMENU |
		WS_MAXIMIZEBOX |
		WS_MINIMIZEBOX
		);

	long ws_ex_and = ~(
		WS_EX_DLGMODALFRAME |
		WS_EX_COMPOSITED |
		WS_EX_WINDOWEDGE |
		WS_EX_CLIENTEDGE |
		WS_EX_LAYERED |
		WS_EX_STATICEDGE |
		WS_EX_TOOLWINDOW |
		WS_EX_APPWINDOW
		);

	SetLastError(0);

	long style = 0, exstyle = 0;
	style = GetWindowLongA(wnd, GWL_STYLE);
	if (style)
	{
		exstyle = GetWindowLongA(wnd, GWL_EXSTYLE);
	}

	unsigned gle = GetLastError();
	if ((!style && !exstyle) && gle)
	{
		lerr("GetWindowLongA failed");
		return 1;
	}

	style &= ws_and;
	exstyle &= ws_ex_and;
	style |= WS_CHILD;
	exstyle |= 0;

	SetLastError(0);
	if (!SetWindowLongA(wnd, GWL_STYLE, style) || !SetWindowLongA(wnd, GWL_EXSTYLE, exstyle))
	{
		if (gle = GetLastError())
		{
			lerr("SetWindowLongA failed");
			return 1;
		}
	}


	if (!GetWindowRect(wnd, &rect))
	{
		lerr("GetWindowRect failed");
		return 1;
	}

	worker = createWorker();

	MapWindowPoints(0, worker, (LPPOINT)&rect, 2);

	if (!SetParent(wnd, worker))
	{
		lerr("SetParent failed");
		return 1;
	}

	ShowWindow(wnd, SW_SHOW);

	if (!SetWindowPos(wnd, 0, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, 0))
	{
		lerr("SetWindowPos failed");
		return 1;
	}

	if (!GetWindowRect(wnd, &rect))
	{
		lerr("GetWindowRect failed");
		return 1;
	}

	HMONITOR mon = MonitorFromPoint(*(POINT*)&rect, MONITOR_DEFAULTTONEAREST);
	if (!mon)
	{
		lerr("MonitorFromPoint failed");
		return 1;
	}

	MONITORINFO mi{};
	mi.cbSize = sizeof(mi);
	if (!GetMonitorInfoA(mon, &mi))
	{
		lerr("GetMonitorInfoA failed");
		return 1;
	}

	MapWindowPoints(0, worker, (LPPOINT)&mi.rcMonitor, 2);

	if (!SetWindowPos(wnd, 0, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, 0))
	{
		lerr("SetWindowPos failed");
		return 1;
	}

	return 0;
}
